package com.example.storebookservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StorebookserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorebookserviceApplication.class, args);
    }

}
